package com.twanl.tokens.events;

import com.twanl.tokens.Tokens;
import com.twanl.tokens.lib.Lib;
import com.twanl.tokens.sql.SQLlib;
import com.twanl.tokens.utils.Strings;
import com.twanl.tokens.utils.UpdateChecker;
import com.twanl.tokens.utils.loadManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Twan on 3/22/2018.
 **/

public class JoinEvent implements Listener {

    private Tokens plugin = Tokens.getPlugin(Tokens.class);
    private Lib lib = new Lib();
    private SQLlib sql = new SQLlib();

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        final Player p = e.getPlayer();


        // Update message
        if (loadManager.update_message()) {
            if (p.hasPermission("tokens.update")) {


                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    public void run() {
                        UpdateChecker checker = new UpdateChecker();

                        if (checker.hasUpdate()) {
                            if (checker.isPreRelease()) {
                                p.sendMessage(Strings.DgrayBS + "                      \n");
                                plugin.nms.sendClickableHovarableMessageURL(p, Strings.red + "Tokens is outdated!", Strings.gold + "Click to go to the download page", "https://www.spigotmc.org/resources/tokens.53944/");
                                p.sendMessage(" \n" +
                                        Strings.yellowB + "[X] YOU ARE IN A PRE RELEASE VERSION [X]\n" +
                                        Strings.white + "Your version: " + plugin.getDescription().getVersion() + "\n" +
                                        Strings.white + "Newest version: " + Strings.green + checker.getUpdatedVersion() + "\n" +
                                        Strings.DgrayBS + "                      ");
                            } else {
                                p.sendMessage(Strings.DgrayBS + "                      \n");
                                plugin.nms.sendClickableHovarableMessageURL(p, Strings.red + "Tokens is outdated!", Strings.gold + "Click to go to the download page", "https://www.spigotmc.org/resources/tokens.53944/");
                                p.sendMessage(" \n" +
                                        Strings.white + "Your version: " + plugin.getDescription().getVersion() + "\n" +
                                        Strings.white + "Newest version: " + Strings.green + checker.getUpdatedVersion() + "\n" +
                                        Strings.DgrayBS + "                      ");
                            }
                        } else {
                            if (checker.isPreRelease()) {
                                p.sendMessage(Strings.DgrayBS + "                      \n" +
                                        Strings.yellowB + "[X] YOU ARE IN A PRE RELEASE VERSION [X]" +
                                        Strings.green + "Tokens is up to date.\n" +
                                        Strings.DgrayBS + "                      ");
                            } else {
                                p.sendMessage(Strings.DgrayBS + "                      \n" +
                                        Strings.green + "Tokens is up to date.\n" +
                                        Strings.DgrayBS + "                      ");
                            }
                        }
                    }
                }, 20);

            }
        }


        if (lib.sqlUse()) {
            try {
                if (!sql.hasAccount(p.getUniqueId())) {
                    sql.addPlayer(p.getUniqueId());
                }
            } catch (Exception e1) {
                p.sendMessage(Strings.prefix + Strings.red + "error, check console!");
                Bukkit.getConsoleSender().sendMessage(Strings.logName + Strings.red + "Probably failed to connect to the mySQL database");
            }
        } else {
            lib.creatAccount(p.getUniqueId());
        }
    }
}
